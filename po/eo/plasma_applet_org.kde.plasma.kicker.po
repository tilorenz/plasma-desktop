# translation of plasma_applet_org.kde.plasma.kicker.pot esperanto
# Copyright (C) 2016 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-desktop package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-12 02:13+0000\n"
"PO-Revision-Date: 2023-04-12 07:00+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Generalo"

#: package/contents/ui/code/tools.js:42
#, kde-format
msgid "Remove from Favorites"
msgstr "Forigi el Favoritoj"

#: package/contents/ui/code/tools.js:46
#, kde-format
msgid "Add to Favorites"
msgstr "Aldoni al Favoritoj"

#: package/contents/ui/code/tools.js:70
#, kde-format
msgid "On All Activities"
msgstr "Pri Ĉiuj Agadoj"

#: package/contents/ui/code/tools.js:120
#, kde-format
msgid "On the Current Activity"
msgstr "Pri la Nuna Agado"

#: package/contents/ui/code/tools.js:134
#, kde-format
msgid "Show in Favorites"
msgstr "Montri en Favoritoj"

#: package/contents/ui/ConfigGeneral.qml:45
#, kde-format
msgid "Icon:"
msgstr "Piktogramo:"

#: package/contents/ui/ConfigGeneral.qml:125
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Elektu…"

#: package/contents/ui/ConfigGeneral.qml:130
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Klara Piktogramo"

#: package/contents/ui/ConfigGeneral.qml:148
#, kde-format
msgid "Show applications as:"
msgstr "Montri aplikojn kiel:"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Name only"
msgstr "Nomo nur"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Description only"
msgstr "Nur priskribo"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Name (Description)"
msgstr "Nomo (Priskribo)"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Description (Name)"
msgstr "Priskribo (Nomo)"

#: package/contents/ui/ConfigGeneral.qml:160
#, kde-format
msgid "Behavior:"
msgstr "Konduto:"

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgid "Sort applications alphabetically"
msgstr "Ordigi aplikojn alfabete"

#: package/contents/ui/ConfigGeneral.qml:170
#, kde-format
msgid "Flatten sub-menus to a single level"
msgstr "Platigi submenuojn al ununura nivelo"

#: package/contents/ui/ConfigGeneral.qml:178
#, kde-format
msgid "Show icons on the root level of the menu"
msgstr "Montri piktogramojn sur la radika nivelo de la menuo"

#: package/contents/ui/ConfigGeneral.qml:188
#, kde-format
msgid "Show categories:"
msgstr "Montri kategoriojn:"

#: package/contents/ui/ConfigGeneral.qml:191
#, kde-format
msgid "Recent applications"
msgstr "Lastatempaj aplikoj"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgid "Often used applications"
msgstr "Ofte uzataj aplikoj"

#: package/contents/ui/ConfigGeneral.qml:199
#, kde-format
msgid "Recent files"
msgstr "Lastatempaj dosieroj"

#: package/contents/ui/ConfigGeneral.qml:200
#, kde-format
msgid "Often used files"
msgstr "Ofte uzataj dosieroj"

#: package/contents/ui/ConfigGeneral.qml:206
#, kde-format
msgid "Sort items in categories by:"
msgstr "Ordigi erojn en kategorioj per:"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Often used]"
msgid "Recently used"
msgstr "Lastatempe uzata"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Ofetn used]"
msgid "Often used"
msgstr "Ofte uzata"

#: package/contents/ui/ConfigGeneral.qml:217
#, kde-format
msgid "Search:"
msgstr "Serĉi:"

#: package/contents/ui/ConfigGeneral.qml:219
#, kde-format
msgid "Expand search to bookmarks, files and emails"
msgstr "Pligrandigi serĉon al legosignoj, dosieroj kaj retpoŝtoj"

#: package/contents/ui/ConfigGeneral.qml:227
#, kde-format
msgid "Align search results to bottom"
msgstr "Vicigi serĉrezultojn malsupre"

#: package/contents/ui/DashboardRepresentation.qml:288
#, kde-format
msgid "Searching for '%1'"
msgstr "Serĉante '%1'"

#: package/contents/ui/DashboardRepresentation.qml:288
#, kde-format
msgctxt "@info:placeholder as in, 'start typing to initiate a search'"
msgid "Type to search…"
msgstr "Tajpu por serĉi…"

#: package/contents/ui/DashboardRepresentation.qml:388
#, kde-format
msgid "Favorites"
msgstr "Plej ŝatataj"

#: package/contents/ui/DashboardRepresentation.qml:606
#: package/contents/ui/DashboardTabBar.qml:42
#, kde-format
msgid "Widgets"
msgstr "Fenestraĵoj"

#: package/contents/ui/DashboardTabBar.qml:31
#, kde-format
msgid "Apps & Docs"
msgstr "Aplikoj kaj Dokumentoj"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Edit Applications…"
msgstr "Redakti Aplikaĵojn…"

#~ msgid "Recent contacts"
#~ msgstr "Lastatempaj kontaktoj"

#~ msgid "Often used contacts"
#~ msgstr "Ofte uzataj kontaktoj"
