# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2008, 2013.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.taskmanager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-26 01:59+0000\n"
"PO-Revision-Date: 2017-05-23 01:00-0400\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Zanata 3.9.6\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr ""

#: package/contents/config/config.qml:18
#, kde-format
msgid "Behavior"
msgstr "התנהגות"

#: package/contents/ui/AudioStream.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr ""

#: package/contents/ui/AudioStream.qml:101
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@action:button"
msgid "Mute"
msgstr "השתק"

#: package/contents/ui/AudioStream.qml:102
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@info:tooltip %1 is the window title"
msgid "Unmute %1"
msgstr "השתק"

#: package/contents/ui/AudioStream.qml:102
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@info:tooltip %1 is the window title"
msgid "Mute %1"
msgstr "השתק"

#: package/contents/ui/Badge.qml:54
#, kde-format
msgctxt "Invalid number of new messages, overlay, keep short"
msgid "—"
msgstr ""

#: package/contents/ui/Badge.qml:56
#, kde-format
msgctxt "Over 9999 new messages, overlay, keep short"
msgid "9,999+"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:33
#, fuzzy, kde-format
#| msgid "General"
msgid "General:"
msgstr "כללי"

#: package/contents/ui/ConfigAppearance.qml:34
#, kde-format
msgid "Show small window previews when hovering over Tasks"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:39
#, kde-format
msgid "Hide other windows when hovering over previews"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:44
#, kde-format
msgid "Mark applications that play audio"
msgstr "סמן יישומים המנגנים שמע"

#: package/contents/ui/ConfigAppearance.qml:52
#, kde-format
msgctxt "@option:check"
msgid "Fill free space on Panel"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:61
#, kde-format
msgid "Maximum columns:"
msgstr "מקסימום עמודות:"

#: package/contents/ui/ConfigAppearance.qml:61
#, kde-format
msgid "Maximum rows:"
msgstr "מספר שורות מירבי:"

#: package/contents/ui/ConfigAppearance.qml:67
#, fuzzy, kde-format
#| msgid "Always arrange tasks in columns of as many rows"
msgid "Always arrange tasks in rows of as many columns"
msgstr "תמיד ארגן משימות בעמודות של הרבה שורות"

#: package/contents/ui/ConfigAppearance.qml:67
#, kde-format
msgid "Always arrange tasks in columns of as many rows"
msgstr "תמיד ארגן משימות בעמודות של הרבה שורות"

#: package/contents/ui/ConfigAppearance.qml:77
#, kde-format
msgid "Spacing between icons:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:81
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr "קטן"

#: package/contents/ui/ConfigAppearance.qml:85
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:89
#, fuzzy, kde-format
#| msgid "Large"
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr "גדול"

#: package/contents/ui/ConfigAppearance.qml:113
#, kde-format
msgctxt "@info:usagetip under a set of radio buttons when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:47
#, fuzzy, kde-format
#| msgid "Grouping:"
msgid "Group:"
msgstr "קיבוץ:"

#: package/contents/ui/ConfigBehavior.qml:50
#, fuzzy, kde-format
#| msgid "Do Not Group"
msgid "Do not group"
msgstr "אל תקבץ"

#: package/contents/ui/ConfigBehavior.qml:50
#, fuzzy, kde-format
#| msgid "By Program Name"
msgid "By program name"
msgstr "לפי שם יישום"

#: package/contents/ui/ConfigBehavior.qml:55
#, kde-format
msgid "Clicking grouped task:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:62
#, fuzzy, kde-format
#| msgid "Cycle through tasks with mouse wheel"
msgctxt "Completes the sentence 'Clicking grouped task cycles through tasks' "
msgid "Cycles through tasks"
msgstr "עבור בין משימות ע\"י גלילה"

#: package/contents/ui/ConfigBehavior.qml:63
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows tooltip window "
"thumbnails' "
msgid "Shows small window previews"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:64
#, kde-format
msgctxt ""
"Completes the sentence 'Clicking grouped task shows windows side by side' "
msgid "Shows large window previews"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:65
#, kde-format
msgctxt "Completes the sentence 'Clicking grouped task shows textual list' "
msgid "Shows textual list"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:73
#, kde-format
msgid ""
"The compositor does not support displaying windows side by side, so a "
"textual list will be displayed instead."
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:83
#, kde-format
msgid "Combine into single button"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:90
#, fuzzy, kde-format
#| msgid "Only group when the task manager is full"
msgid "Group only when the Task Manager is full"
msgstr "קבץ רק ששורת המשימות מלאה"

#: package/contents/ui/ConfigBehavior.qml:101
#, fuzzy, kde-format
#| msgid "Sorting:"
msgid "Sort:"
msgstr "מיון:"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "Do not sort"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "Manually"
msgstr "ידנית"

#: package/contents/ui/ConfigBehavior.qml:104
#, kde-format
msgid "Alphabetically"
msgstr "לפי סדר הא\"ב"

#: package/contents/ui/ConfigBehavior.qml:104
#, fuzzy, kde-format
#| msgid "By Desktop"
msgid "By desktop"
msgstr "לפי שולחן עבודה"

#: package/contents/ui/ConfigBehavior.qml:104
#, fuzzy, kde-format
#| msgid "By Activity"
msgid "By activity"
msgstr "לפי פעילות"

#: package/contents/ui/ConfigBehavior.qml:110
#, kde-format
msgid "Keep launchers separate"
msgstr "שמור משגרים מופרדים"

#: package/contents/ui/ConfigBehavior.qml:121
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Clicking active task:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:122
#, kde-format
msgctxt "Part of a sentence: 'Clicking active task minimizes the task'"
msgid "Minimizes the task"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:127
#, kde-format
msgid "Middle-clicking any task:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:131
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task does nothing'"
msgid "Does nothing"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:132
#, fuzzy, kde-format
#| msgid "Close Window or Group"
msgctxt "Part of a sentence: 'Middle-clicking any task closes window or group'"
msgid "Closes window or group"
msgstr "סגור חלון או קבוצת חלונות"

#: package/contents/ui/ConfigBehavior.qml:133
#, fuzzy, kde-format
#| msgid "New Instance"
msgctxt "Part of a sentence: 'Middle-clicking any task opens a new window'"
msgid "Opens a new window"
msgstr "פתח מופע חדש"

#: package/contents/ui/ConfigBehavior.qml:134
#, fuzzy, kde-format
#| msgid "Minimize/Restore Window or Group"
msgctxt ""
"Part of a sentence: 'Middle-clicking any task minimizes/restores window or "
"group'"
msgid "Minimizes/Restores window or group"
msgstr "מזער/שחזר חלון או קבוצה"

#: package/contents/ui/ConfigBehavior.qml:135
#, kde-format
msgctxt "Part of a sentence: 'Middle-clicking any task toggles grouping'"
msgid "Toggles grouping"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:136
#, fuzzy, kde-format
#| msgid "Show only tasks from the current desktop"
msgctxt ""
"Part of a sentence: 'Middle-clicking any task brings it to the current "
"virtual desktop'"
msgid "Brings it to the current virtual desktop"
msgstr "הראה רק משימות משולחן העבודה הנוכחי"

#: package/contents/ui/ConfigBehavior.qml:146
#, kde-format
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Mouse wheel:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:147
#, fuzzy, kde-format
#| msgid "Cycle through tasks with mouse wheel"
msgctxt "Part of a sentence: 'Mouse wheel cycles through tasks'"
msgid "Cycles through tasks"
msgstr "עבור בין משימות ע\"י גלילה"

#: package/contents/ui/ConfigBehavior.qml:156
#, kde-format
msgid "Skip minimized tasks"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:167
#, fuzzy, kde-format
#| msgid "Show tooltips"
msgid "Show only tasks:"
msgstr "הצג חלוניות מידע"

#: package/contents/ui/ConfigBehavior.qml:168
#, fuzzy, kde-format
#| msgid "Show only tasks from the current screen"
msgid "From current screen"
msgstr "הראה רק משימות מהמסך הנוכחי"

#: package/contents/ui/ConfigBehavior.qml:173
#, fuzzy, kde-format
#| msgid "Move &To Current Desktop"
msgid "From current desktop"
msgstr "העבר לשולחן עבודה הנוכחי"

#: package/contents/ui/ConfigBehavior.qml:178
#, fuzzy, kde-format
#| msgid "Add To Current Activity"
msgid "From current activity"
msgstr "הוסף לפעילות הנוכחית"

#: package/contents/ui/ConfigBehavior.qml:183
#, fuzzy, kde-format
#| msgid "Show only tasks that are minimized"
msgid "That are minimized"
msgstr "הראה רק משימות ממוזערות"

#: package/contents/ui/ConfigBehavior.qml:192
#, kde-format
msgid "When panel is hidden:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:193
#, kde-format
msgid "Unhide when a window wants attention"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:205
#, kde-format
msgid "New tasks appear:"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:209
#, kde-format
msgid "On the bottom"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:213
#: package/contents/ui/ConfigBehavior.qml:233
#, kde-format
msgid "To the right"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:215
#: package/contents/ui/ConfigBehavior.qml:231
#, kde-format
msgid "To the left"
msgstr ""

#: package/contents/ui/ConfigBehavior.qml:227
#, kde-format
msgid "On the Top"
msgstr ""

#: package/contents/ui/ContextMenu.qml:93
#, kde-format
msgid "Places"
msgstr ""

#: package/contents/ui/ContextMenu.qml:98
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgid "Recent Files"
msgstr "נקה מסמכים אחרונים"

#: package/contents/ui/ContextMenu.qml:103
#, fuzzy, kde-format
#| msgid "More Actions"
msgid "Actions"
msgstr "עוד פעולות"

#: package/contents/ui/ContextMenu.qml:168
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "הרצועה הקודמת"

#: package/contents/ui/ContextMenu.qml:182
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "השהה"

#: package/contents/ui/ContextMenu.qml:182
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "נגן"

#: package/contents/ui/ContextMenu.qml:200
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "הרצועה הבאה"

#: package/contents/ui/ContextMenu.qml:211
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "עצור"

#: package/contents/ui/ContextMenu.qml:231
#, kde-format
msgctxt "Quit media player app"
msgid "Quit"
msgstr "צא"

#: package/contents/ui/ContextMenu.qml:246
#, kde-format
msgctxt "Open or bring to the front window of media player app"
msgid "Restore"
msgstr "שחזר"

#: package/contents/ui/ContextMenu.qml:272
#, kde-format
msgid "Mute"
msgstr "השתק"

#: package/contents/ui/ContextMenu.qml:283
#, kde-format
msgid "Open New Window"
msgstr ""

#: package/contents/ui/ContextMenu.qml:299
#, fuzzy, kde-format
#| msgid "Move To &Desktop"
msgid "Move to &Desktop"
msgstr "העבר לשולחן עבודה"

#: package/contents/ui/ContextMenu.qml:323
#, kde-format
msgid "Move &To Current Desktop"
msgstr "העבר לשולחן עבודה הנוכחי"

#: package/contents/ui/ContextMenu.qml:332
#, kde-format
msgid "&All Desktops"
msgstr "&כל שולחנות העבודה"

#: package/contents/ui/ContextMenu.qml:346
#, kde-format
msgctxt "1 = number of desktop, 2 = desktop name"
msgid "&%1 %2"
msgstr "&%1 %2"

#: package/contents/ui/ContextMenu.qml:360
#, kde-format
msgid "&New Desktop"
msgstr "שולחן עבודה חדש"

#: package/contents/ui/ContextMenu.qml:380
#, fuzzy, kde-format
#| msgid "All Activities"
msgid "Show in &Activities"
msgstr "כל הפעילויות"

#: package/contents/ui/ContextMenu.qml:404
#, kde-format
msgid "Add To Current Activity"
msgstr "הוסף לפעילות הנוכחית"

#: package/contents/ui/ContextMenu.qml:414
#, kde-format
msgid "All Activities"
msgstr "כל הפעילויות"

#: package/contents/ui/ContextMenu.qml:472
#, fuzzy, kde-format
#| msgid "Move To &Desktop"
msgid "Move to %1"
msgstr "העבר לשולחן עבודה"

#: package/contents/ui/ContextMenu.qml:500
#: package/contents/ui/ContextMenu.qml:517
#, kde-format
msgid "&Pin to Task Manager"
msgstr ""

#: package/contents/ui/ContextMenu.qml:570
#, kde-format
msgid "On All Activities"
msgstr "בכל הפעילויות"

#: package/contents/ui/ContextMenu.qml:576
#, kde-format
msgid "On The Current Activity"
msgstr "בפעילות הנוכחית"

#: package/contents/ui/ContextMenu.qml:601
#, kde-format
msgid "Unpin from Task Manager"
msgstr ""

#: package/contents/ui/ContextMenu.qml:616
#, kde-format
msgid "More"
msgstr ""

#: package/contents/ui/ContextMenu.qml:625
#, kde-format
msgid "&Move"
msgstr "ה&זז"

#: package/contents/ui/ContextMenu.qml:634
#, kde-format
msgid "Re&size"
msgstr "שנה &גודל"

#: package/contents/ui/ContextMenu.qml:648
#, kde-format
msgid "Ma&ximize"
msgstr "&הגדל"

#: package/contents/ui/ContextMenu.qml:662
#, kde-format
msgid "Mi&nimize"
msgstr "&מזער"

#: package/contents/ui/ContextMenu.qml:672
#, kde-format
msgid "Keep &Above Others"
msgstr "שמור מעל אחרים"

#: package/contents/ui/ContextMenu.qml:682
#, kde-format
msgid "Keep &Below Others"
msgstr "שמור מתחת אחרים"

#: package/contents/ui/ContextMenu.qml:694
#, kde-format
msgid "&Fullscreen"
msgstr "&מסך מלא"

#: package/contents/ui/ContextMenu.qml:706
#, kde-format
msgid "&Shade"
msgstr "האפל"

#: package/contents/ui/ContextMenu.qml:722
#, kde-format
msgid "Allow this program to be grouped"
msgstr "אפשר ליישום זה להתקבץ"

#: package/contents/ui/ContextMenu.qml:770
#, fuzzy, kde-format
#| msgid "&Close"
msgctxt "@item:inmenu"
msgid "&Close All"
msgstr "&סגור"

#: package/contents/ui/ContextMenu.qml:770
#, kde-format
msgid "&Close"
msgstr "&סגור"

#: package/contents/ui/Task.qml:78
#, kde-format
msgctxt "@info:usagetip %1 application name"
msgid "Launch %1"
msgstr ""

#: package/contents/ui/Task.qml:83
#, kde-format
msgctxt "@info:tooltip"
msgid "There is %1 new message."
msgid_plural "There are %1 new messages."
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/Task.qml:92
#, fuzzy, kde-format
#| msgid "Show tooltips"
msgctxt "@info:usagetip %1 task name"
msgid "Show Task tooltip for %1"
msgstr "הצג חלוניות מידע"

#: package/contents/ui/Task.qml:98
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Show windows side by side for %1"
msgstr ""

#: package/contents/ui/Task.qml:103
#, kde-format
msgctxt "@info:usagetip %1 task name"
msgid "Open textual list of windows for %1"
msgstr ""

#: package/contents/ui/Task.qml:107
#, kde-format
msgid "Activate %1"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:356
#, kde-format
msgctxt "button to unmute app"
msgid "Unmute %1"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:357
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "button to mute app"
msgid "Mute %1"
msgstr "השתק"

#: package/contents/ui/ToolTipInstance.qml:380
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:396
#, fuzzy, kde-format
#| msgctxt "1 = number of desktop, 2 = desktop name"
#| msgid "&%1 %2"
msgctxt "volume percentage"
msgid "%1%"
msgstr "&%1 %2"

#: package/contents/ui/ToolTipInstance.qml:399
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:423
#, fuzzy, kde-format
#| msgid "On %1"
msgctxt "Comma-separated list of desktops"
msgid "On %1"
msgstr "ב־%1"

#: package/contents/ui/ToolTipInstance.qml:426
#, kde-format
msgctxt "Comma-separated list of desktops"
msgid "Pinned to all desktops"
msgstr ""

#: package/contents/ui/ToolTipInstance.qml:437
#, kde-format
msgctxt "Which virtual desktop a window is currently on"
msgid "Available on all activities"
msgstr "זמין בכל הפעילויות"

#: package/contents/ui/ToolTipInstance.qml:459
#, kde-format
msgctxt "Activities a window is currently on (apart from the current one)"
msgid "Also available on %1"
msgstr "זמין גם ב %1"

#: package/contents/ui/ToolTipInstance.qml:463
#, kde-format
msgctxt "Which activities a window is currently on"
msgid "Available on %1"
msgstr "זמין ב %1"

#: plugin/backend.cpp:351
#, kde-format
msgctxt "Show all user Places"
msgid "%1 more Place"
msgid_plural "%1 more Places"
msgstr[0] "עוד מקום אחד"
msgstr[1] "עוד %1 מקומות"

#: plugin/backend.cpp:447
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgid "Recent Downloads"
msgstr "נקה מסמכים אחרונים"

#: plugin/backend.cpp:449
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgid "Recent Connections"
msgstr "נקה מסמכים אחרונים"

#: plugin/backend.cpp:451
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgid "Recent Places"
msgstr "נקה מסמכים אחרונים"

#: plugin/backend.cpp:460
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Downloads"
msgstr "נקה מסמכים אחרונים"

#: plugin/backend.cpp:462
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Connections"
msgstr "נקה מסמכים אחרונים"

#: plugin/backend.cpp:464
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Places"
msgstr "נקה מסמכים אחרונים"

#: plugin/backend.cpp:466
#, fuzzy, kde-format
#| msgid "Forget Recent Documents"
msgctxt "@action:inmenu"
msgid "Forget Recent Files"
msgstr "נקה מסמכים אחרונים"

#~ msgid "Show tooltips"
#~ msgstr "הצג חלוניות מידע"

#~ msgid "Icon size:"
#~ msgstr "גודל סמל:"

#~ msgid "Start New Instance"
#~ msgstr "פתח חלון חדש"

#~ msgid "More Actions"
#~ msgstr "עוד פעולות"

#, fuzzy
#~| msgid "Cycle through tasks with mouse wheel"
#~ msgid "Cycle through tasks"
#~ msgstr "עבור בין משימות ע\"י גלילה"

#~ msgid "On middle-click:"
#~ msgstr "בלחיצה על הגלגלת:"

#~ msgctxt "The click action"
#~ msgid "None"
#~ msgstr "כלום"

#~ msgctxt "When clicking it would toggle grouping windows of a specific app"
#~ msgid "Group/Ungroup"
#~ msgstr "קבץ/בטל קיבוץ"

#~ msgid "Open groups in popups"
#~ msgstr "פתח קבוצה בחלונות קופצים"

#, fuzzy
#~| msgid "Filters"
#~ msgid "Filter:"
#~ msgstr "מסננים"

#~ msgid "Show only tasks from the current desktop"
#~ msgstr "הראה רק משימות משולחן העבודה הנוכחי"

#~ msgid "Show only tasks from the current activity"
#~ msgstr "הראה רק משימות מהפעילות הנוכחית"

#, fuzzy
#~| msgid "Always arrange tasks in rows of as many columns"
#~ msgid "Always arrange tasks in as many rows as columns"
#~ msgstr "תמיד ארגן משימות בשורות של הרבה עמודות"

#, fuzzy
#~| msgid "Always arrange tasks in rows of as many columns"
#~ msgid "Always arrange tasks in as many columns as rows"
#~ msgstr "תמיד ארגן משימות בשורות של הרבה עמודות"

#, fuzzy
#~| msgid "Move To &Activity"
#~ msgid "Move to &Activity"
#~ msgstr "העבר ל&פעילות"

#~ msgid "Show progress and status information in task buttons"
#~ msgstr "הצג מידע על התקדמות ופרטים בלחצני משימות"

#~ msgctxt ""
#~ "Toggle action for showing a launcher button while the application is not "
#~ "running"
#~ msgid "&Pin"
#~ msgstr "&"

#~ msgid "&Pin"
#~ msgstr "&הצמד"

#~ msgctxt ""
#~ "Remove launcher button for application shown while it is not running"
#~ msgid "Unpin"
#~ msgstr "בטל הצמדה"

#~ msgid "Arrangement"
#~ msgstr "סדר"

#~ msgid "Highlight windows"
#~ msgstr "הדגש חלונות"

#~ msgid "Grouping and Sorting"
#~ msgstr "קיבוץ ומיון"

#~ msgid "Do Not Sort"
#~ msgstr "אל תמיין"
